/*    1. $ pwd > ./resultados/01_solucion
    2. $ ls -d /etc/t* | sort -r > ./resultados/02_solucion
	$ ls -drl /etc/t* > ./resultados/02_solucion
    3. $ ls -d /dev/tty?? > ./resultados/03_solucion
    4. $ ls /dev/tty*[1,2,3,4] > ./resultados/04_solucion
    5. $ ls -ad /dev/t??S?? > ./resultados/05_solucion
    6. $ ls -Ad /usr/* > ./resultados/06_solucion
    7. $ ls -a / > ./resultados/07_solucion
    8. $ ls -d /etc/[^t]* > ./resultados/08_solucion
    9. $ ls -R /usr > ./resultados/09_solucion &
    10. $  grep config ./resultados/09_solucion > ./resultados/10_solucion
    11. $ wc ./resultados/09_solucion > ./resultados/11_solucion && wc ./resultados/10_solucion >> ./resultados/11_solucion
    12. date > ./resultados/12_solucion
    13. $ mkdir dir1 dir2 dir3 ./dir1/dir11 ./dir3/dir31 ./dir3/dir31/dir312 ./dir3/dir31/dir311 && ls -R . > ./resultados/13_solucion
    14. $ cp -r /etc/rcS.d/* ./dir3/dir31 && ls -la ./dir3/dir31 > ./resultados/14_solucion
    15. $  ln -s ~/examen_20191004/dir1 ~/examen_20191004/dir3/enlacedir1 && ls -la ~/examen_20191004/dir3/* > ~/examen_20191004/resultados/15_solucion
    16. $ cd ~/examen_20191004/dir3 && mkdir enlacedir1/nuevo1 && ls -la ~/examen_20191004/dir1/* > ~/examen_20191004/resultados/16_solucion
    17. $ ls -la ~/examen_20191004/dir2 > ~/examen_20191004/resultados/17_solucion && chmod ugo-w ~/examen_20191004/dir2 && ls -la ~/examen_20191004/dir2 >> ~/examen_20191004/resultados/17_solucion
    18. $ find . -name S* -print > ./resultados/18_solucion
    19. $ find . -name S* -print | cut -d/ -f4 > ./resultados/19_solucion
    20. $ file ./dir3/dir31/* > ./resultados/20_solucion
    21. $ ps -ef | grep root > ./resultados/21_solucion
    22. $ ps -ef | grep guillegv > ./resultados/22_solucion
    23. $ free > ./resultados/23_solucion
    24. $ cat ficheroquenoexiste 2> ./resultados/24_solucion
    25. $ touch ./dir2/fich21 ./dir2/fich22 2> ./resultados/25_solucion
    26. $ chmod u+w dir2 && touch ./dir2/fich21 ./dir2/fich22 && ls -la ./dir2/* > ./resultados/26_solucion
    27. $ time ls -R /usr >/dev/null
    28. $ env > ./resultados/28_solucion
    29. $ echo $PS1 > ./resultados/29_solucion
    30. $ df -k > ./resultados/30_solucion
    31. $ cat /etc/passwd > ~/examen_20191004/resultados/31_solucion
    32. $ ls -R ~/examen_20191004 > ~/examen_20191004/resultados/32_solucion
    33. $ tar cvf examen.tar .; $ gzip examen.tar;*/
