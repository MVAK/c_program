#!/bin/bash

read -a numeros #lee los parametros y los mete en el array
for numero in ${numeros[*]} #recorremos los elementos del array
do 
resultado=`expr numero \* numero` #averiguamos si es entero
	if [[ $resultado -eq 0 ]] #si el resultado es entero entonces
then
	echo "la tabla de multiplicar del ${numeros[*]} es: \n "
	for ((i=1;i<=10;i++)) #vamos incrementando la i que es el numero de 1 a 10
	do 
		resultadofinal=$((${numeros[*]}*$i)) #calculamos el resultado de la multiplicacion
	echo "${numeros[*]} * $i = $resultadofinal" #lo mostramos por pantalla
done
else echo "el elemento ${numeros[*]} no se procesa porque no es numerico"
fi
done 
