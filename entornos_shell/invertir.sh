#!/bin/bash
read -p "Introduce una cadena de caracteres: " string
let len=${#string}
for ((i = $len - 1; i >= 0; i--))
do
    # "${string:$i:1}"extrae un solo caracter de la cadena.
    reverse="$reverse${string:$i:1}"
done
echo "$reverse"
