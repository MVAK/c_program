#!/bin/bash
# numerosprimos.sh: Genera los números primos, usando arrays.
# Usamos un método consistente en probar cada número candidato con sus posibles divisores previos usando el operador módulo '%'

# Compruebo número de parámetros.
if [[ $# -ne 3 ]]; then
    #Número de parámetros != 2. Mostramos mensaje de error y salimos del programa.
    echo "Número de parámetros incorrecto. Se esperan 3 parámetros numericos."
    echo "USO: numerosprimos.sh <limite_inferior> <limite_superior> <primos_por_linea>"
        exit 1
fi

#Intento operar con los parámetros recibidos para ver si son operables.
mensaje_error=$(expr $1 \* $2 \* $3 2>&1)
#Si el comando se ha ejecutado con error tendremos en $? un valor mayor que cero y en mensaje_error el motivo.
if [[ $? -gt 0 ]]; then
    # Algún parámetro no es un número.
    echo "$mensaje_error. Se esperan parámetros numericos."
    echo "USO: numerosprimos.sh <limite_inferior> <limite_superior> <primos_por_linea>"
        exit 2
fi

LIMIT_INF=$1
LIMIT_SUP=$2       # Números primos, 2 ... 1000.
PRIMOS_POR_LINEA=$3

if test ${LIMIT_SUP} -le ${LIMIT_INF}
then
    # ERROR: Límite superior es menor o igual que el limite inferior.
    printf "ERROR: Límite superior %d es menor o igual que el limite inferior %d\n" ${LIMIT_SUP} ${LIMIT_INF}
    echo "USO: numerosprimos.sh <limite_inferior> <limite_superior> <primos_por_linea>"
        exit 3
fi

declare -a primos;
declare -i num_primos=0;

for((n=${LIMIT_INF};n<=${LIMIT_SUP};n++))
do
        let es_primo=1                                  # Mientras no sea divisible se considerará primo.
        for((i=2;i<$n;i++))
        do
                (( i * i > n )) && break   # Optimización. Si el cuadrado del divisor (i) es mayor que el dividendo (n) no hay que seguir mirando divisores.
                (( n % i )) && continue    # Saltamos los números no primos, identificados cuando el operador modulo '%' da cero de resto.
                let es_primo=0 && break                 # Tiene un divisor, luego no es primo y podemos salir del bucle.
        done
        if [[ $es_primo -eq 1 ]]
        then
                primos[$num_primos]=$n
                # let "num_primos = $num_primos + 1"
                (( num_primos++ ))
        fi
done
printf "El número de números primos entre [%d y %d] es %d \n" ${LIMIT_INF} ${LIMIT_SUP} ${#primos[@]}
# echo ${primos[@]}
# Recorremos el array para formatear la salida de forma elegante.
declare -i indice=0;
# Imprimo linea de cabecera.
echo -n "-" # Una posición por tubería inicial
for n in `seq $PRIMOS_POR_LINEA` # 5 posiciones por cada numero primo de una línea.
do
        echo -n "-----"
done
for primo in "${primos[@]}"
do
    if (( indice % PRIMOS_POR_LINEA == 0 ))
        then
                # Linea nueva.
                printf "\n|"
        fi
        # Accedemos a cada elemento como $i
    # echo $i
        printf "%4d|" $primo
        (( indice++ ))
done
printf "\n-"
# Imprimo linea de pie.
for n in `seq $PRIMOS_POR_LINEA`
do
        echo -n "-----"
done
printf "\n"


