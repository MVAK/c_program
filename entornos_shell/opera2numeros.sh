#!/bin/bash
<<ENUNCIADO
Script que debe recibir por parámetros dos números, comprobar que se han recibido exactamente 2 parámetros y son numéricos.
Salga del programa con un código de error 1 si no son 2 parámetros y código de error 2 si algún parámetro no es numérico.
En ambos casos se mostrará mensaje de error y el uso correcto de ejecución del script “USO: opera2numeros.sh numero1 numero2”.
Si se han recibido 2 parámetros numéricos, si numero1 es menor que numero2 se suman, si numero1 es mayor que numero2 se restan y si son iguales se multiplican.
En todos los casos se muestra la operación realizada y el resultado obtenido devolviendo el script un código de salida 0.
FIN
ENUNCIADO

# Compruebo número de parámetros.
if [[ $# -ne 2 ]]; then
    #Número de parámetros != 2. Mostramos mensaje de error y salimos del programa.
    echo "Número de parámetros incorrecto. Se esperan 2 parámetros numericos."
    echo "USO: opera2numeros.sh <numero1> <numero2>"
	exit 1
fi

#Intento operar con los parámetros recibidos para ver si son operables.
mensaje_error=$(expr $1 \* $2 2>&1)
#Si el comando se ha ejecutado con error tendremos en $? un valor mayor que cero y en mensaje_error el motivo.
if [[ $? -gt 0 ]]; then
    # Algún parámetro no es un número.
    echo "$mensaje_error. Se esperan parámetros numericos."
    echo "USO: opera2numeros.sh <numero1> <numero2>"
	exit 2
fi

<<CONTROL_NO_TAN_PERFECTO
# declaro una variable de tipo entero y luego intento asignarle el primer parámetro, si da error entonces no es numérica
declare -i num1
num1=$1
if [[ $? -gt 0 ]]; then
    # Primer parámetro no es un número.
    echo "El primer parámetro no es un número. Se esperan parámetros numericos."
    echo "USO: opera2numeros.sh <numero1> <numero2>"
	exit 2
fi

# declaro una variable de tipo entero y luego intento asignarle el primer parámetro, si da error entonces no es numérica
declare -i num2
num2=$2
if [[ $? -gt 0 ]]; then
    # Segundo parámetro no es un número.
    echo "El segundo parámetro no es un número. Se esperan parámetros numericos."
    echo "USO: opera2numeros.sh <numero1> <numero2>"
	exit 2
fi
CONTROL_NO_TAN_PERFECTO

<<CONTROL_CON_EXPRESIONES_REGULARES
# Este control es más perfecto (con expresiones regulares) que no hemos visto.
validate_number='^-?[0-9]+([.][0-9]+)?$';
if ! [[ $1 =~ $validate_number ]]; then
    # Primer parámetro no es un número.
    echo "El primer parámetro no es un número. Se esperan parámetros numericos."
    echo "USO: opera2numeros.sh <numero1> <numero2>"
	exit 2
fi
if ! [[ $1 =~ $validate_number ]]; then
    # Segundo parámetro no es un número.
    echo "El segundo parámetro no es un número. Se esperan parámetros numericos."
    echo "USO: opera2numeros.sh <numero1> <numero2>"
	exit 2
fi
CONTROL_CON_EXPRESIONES_REGULARES

# Parámetros de entrada correctos.
if test $1 -lt $2
then
        echo El primer parámetro $1 es menor que el segundo $2.
        echo Realizamos la operación suma.
        echo "$1 + $2 = " `expr $1 + $2`
elif test $1 -gt $2
then
        echo El primer parámetro $1 es mayor que el segundo $2.
        echo Realizamos la operación resta.
        echo "$1 - $2 = " `expr $1 - $2`
else
        echo Los parámetros $1 y $2 son iguales.
        echo Realizamos la operación multiplicación.
        echo "$1 * $2 = " `expr $1 \* $2`
fi


