#!/bin/bash
<<ENUNCIADO
Desarrollar un Shell Script llamado tablamultiplicar.sh que no reciba parámetros. En cambio, el script solicitará al usuario la introducción de un número indeterminado de elementos numéricos separados por espacio que serán asignados a una variable array.
El script debe recorrer los elementos del array en un bucle, eliminar del array los elementos que no sean numéricos mostrando un mensaje “El elemento X no se procesa porque no es numérico”. Con los elementos numéricos procesarlos mostrando el mensaje “La tabla de multiplicar del <número> es:” y mostrando a continuación la tabla de multiplicar de dicho número de forma sencilla usando alguna estructura en bucle según se muestra en el ejemplo.
Al final se debe mostrar el numero de tablas que se solicitó procesar y el número de tablas finalmente procesadas.
ENUNCIADO

# Declaro el array tablas
declare -a tablas
#  Todos los acciones en este script trataran la variable tablas como array
echo "Introduce las tablas de multiplicar que quieras visualizar separadas una de otra por un espacio:"
read -a tablas    # Leemos las tablas y se las asignamos al array tablas.
echo # Línea en blanco.
numero_tablas=${#tablas[@]}
index=0
while [ "$index" -lt "$numero_tablas" ]
do    # recorro todos los elementos del array.
  tabla=${tablas[$index]}
  #Intento operar con el elemento de la tabla para ver si es operables (numerico).
  mensaje_error=$(expr $tabla \* $tabla 2>&1)
  #Si el comando se ha ejecutado con error tendremos en $? un valor mayor que cero y en mensaje_error el motivo.
  if [[ $? -gt 0 ]]; then
    # El elemento no es un número luego lo borro del array.
	echo "El elemento $tabla no se procesa porque no es numérico."
	unset tablas[$index]
  else
	echo La tabla de multiplicar del $tabla es:
	for HOST in $(seq 1 10)
	do
		let resultado=$tabla*$HOST
		echo $tabla x $HOST = $resultado
	done	
  fi  
  let "index = $index + 1"
  #    ((index++))
done # del while
echo
echo "Número de tablas de multiplicar a procesar = $numero_tablas"
numero_tablas=${#tablas[@]}
echo "Número de tablas de multiplicar procesadas = $numero_tablas"
exit 0


