function CifrasNumero() 
{
	var numero,
	    resultado,
	    cifras,
	    potencia,
	    cifra,
	    i

    /*  Bucle cantidad cifras, potencia 100000, 
    /*  Bucle dividiendo el número y cogiendo la cifra */
	numero = parseInt(document.getElementById('numero').value)
	resultado = document.getElementById('resultado')
	cifras = 0
	potencia = 1
	do
	{   
		numero = Math.trunc(numero / 10)
		cifras++
		potencia = potencia * 10
	}while (numero>0)
	potencia = potencia / 10
    /* Ejemplo para Numero=2765, cifras=4 potencia=10000 */
    numero = parseInt(document.getElementById('numero').value)
    resultado.innerHTML = ""
            
    for(i=0;i<cifras;i++)
    {
        /* 2765 ---> Potencia=1000
           2765 / 1000 = 2.765 -->  2 % 10 --> 2 
           2765 / 100  = 27.65 --> 27 % 10 --> 7  */
        cifra = Math.trunc(numero/potencia) % 10
        potencia = potencia / 10
        resultado.innerHTML += "Cifra de " + (i+1) + " es " +
                               cifra + " <br>"
    }
}

function Potencia()
{
	var base,
	    exponente,
	    resultado,
	    potencia,
	    contador

	base = parseInt(document.getElementById('base').value)
	exponente = parseInt(document.getElementById('exponente').value)
	resultado = document.getElementById('resultadoPotencia')
    /* Base=2  Exponente=5
       1 * 2 * 2 * 2 * 2 * 2  / 0 + 2 + 2 + 2 + 2 + 2 
       1 -> 1 * 2 = 2 
       2 -> 2 * 2 = 4  
       3 -> 4 * 2 = 8  
       4 -> 8 * 2 = 16 
       Base=2 Exponente=0
       potencia = 1 
    */
    /*  --- Versión for --- */
    potencia = 1
    for(contador=0;contador<exponente;contador++)
    {
    	potencia = potencia * base
    }
    resultado.innerHTML = "El número " + base + " elevado a "  +
                           exponente + " es " + potencia

    /*  --- Versión while --- */
    /*
    potencia = 1
    contador = 0
    while (contador<exponente)
    {
    	potencia = potencia * base
    	contador++
    }
    */

    /*  --- Versión do-while --- */
    /*   NO FUNCIONA PARA EXPONENTE = 0 * /
    potencia = 1
    contador = 0
    do{
    	potencia = potencia * base
    	contador++
    }while (contador<exponente)
    */
}
/* Devuelve el número inverso al pasado como parámetro 
   Ej. Numero = 4523 --> devuelve 3254 
*/
function CalculaInverso(numero)
{
	var cifra,
	    numeroInverso

    numeroInverso = 0
    do
    {
    	cifra = numero % 10 
    	numero = Math.trunc(numero/10)
    	numeroInverso = (numeroInverso*10) + cifra
    }while (numero>0)
    return numeroInverso
}

function Inverso()
{
	var numero,
	    resultado,
	    cifra,
	    numeroInverso

	numero = parseInt(document.getElementById('numeroI').value)
	resultado = document.getElementById('resultadoInverso')
    /*
      2346  --> 6432 

      1. 2346 % 10 --> 6   2346 / 10 = 234  var = (0*10)+6   = 6
      2. 234  % 10 --> 4   234  / 10 = 23   var = (6*10)+4   = 64
      3. 23   % 10 --> 3   23   / 10 = 2    var = (64*10)+3  = 643
      4. 2    % 10 --> 2   2    / 10 = 0    var = (643*10)+2 = 6432
    */
    numeroInverso = 0
    do
    {
    	cifra = numero % 10 
    	numero = Math.trunc(numero/10)
    	numeroInverso = (numeroInverso*10) + cifra
    }while (numero>0)
    resultado.innerHTML = "El número inverso es " + numeroInverso
}
function EsCapicua() 
{
	var numero,
	    numeroCopia,
	    resultado,
	    cifra,
	    numeroInverso
	
	numero = parseInt(document.getElementById('numeroC').value)
	resultado = document.getElementById('resultadoCapicua')
	/*
	    2452 --> 2542 

        2452 == 2542 --> Si es capicua
        2452 != 2542 --> No es capicua
	*/

    /* ----- Versión con código utilizando función
                   CalculaInverso                   --- */
	numeroCopia = numero
	numeroInverso = CalculaInverso(numero)

    /* ----- Versión con código escrito --- */
    /*	
	numeroCopia = numero
    numeroInverso = 0
    do
    {
    	cifra = numero % 10 
    	numero = Math.trunc(numero/10)
    	numeroInverso = (numeroInverso*10) + cifra
    }while (numero>0)
    */
    if (numeroCopia==numeroInverso)
    	resultado.innerHTML = "El número sí es capicua"
    else
    	resultado.innerHTML = "El número no es capicua"
}
function FechaLargo()
{
	var dia,
	    mes,
	    anio,
	    resultado,
	    meses = [ "Enero",      "Febrero", "Marzo",     "Abril", 
	              "Mayo",       "Junio",   "Julio",     "Agosto",
	              "Septiembre", "Octubre", "Noviembre", "Diciembre"
	              ]

    dia = parseInt(document.getElementById('dia').value)
    mes = parseInt(document.getElementById('mes').value)
    anio = parseInt(document.getElementById('anio').value)
	resultado = document.getElementById('resultadoFechaLargo')
    /*               0        1          11  
        meses --> "Enero","Febrero",..."Diciembre"
        meses[0] --> "Enero"
        
        mes = 2 --> meses[1] --> "Febrero"
        meses[mes-1]
    */
    resultado.innerHTML = "Dia " + dia + " de " + meses[mes-1] + 
                          " del año " + anio
}
function DiaSiguiente()
{
	var dia,
	    mes,
	    anio,
	    resultado,
	    meses = [ "Enero",      "Febrero", "Marzo",     "Abril", 
	              "Mayo",       "Junio",   "Julio",     "Agosto",
	              "Septiembre", "Octubre", "Noviembre", "Diciembre"
	              ]
    dia = parseInt(document.getElementById('dia').value)
    mes = parseInt(document.getElementById('mes').value)
    anio = parseInt(document.getElementById('anio').value)
	resultado = document.getElementById('resultadoDiaSiguiente')

	/*
	    meses 30            (30)--> dia=1 mes++
	    meses 31 (no 12)    (31)--> dia=1 mes++
	    meses 12            (31)--> dia=1 mes=1 anio++
	    mes 2 y bisiesto    (29)--> dia=1 mes++
	    mes 2 y no bisiesto (28)--> dia=1 mes++
	    resto                -- --> dia++

                  0   1   2  3          11   
        meses = {31, 28, 31, 30, ...... 31}  
        
        30/04/2020

        ultimo = meses[mes-1]   30
        if (dia==ultimo)
	*/
}

