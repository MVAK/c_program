function TiposFijos() 
{
	var parrafoVariable,
	    nombre1 = "Ana Ruiz",
	    nombre2 = "Juan Pérez",
	    numero1 = 23,
	    numero2 = 45
    /* Con esta instrucción accedemos al párrafo de HTML
       cuyo ID es resultadoFijo. Una vez almacenamos este
       párrafo en la variable parrafoVariable podemos cambiar 
       cualquier de sus propiedades. Como por ejemplo, la
       propiedad innerHTML 
    */
	parrafoVariable = document.getElementById('resultadoFijo')
	parrafoVariable.innerHTML = "Variables de tipo fijo <br>";
	parrafoVariable.innerHTML += " Tipo String <br>";
	parrafoVariable.innerHTML += "Nombre1 es: " + nombre1 + "<br>"
	parrafoVariable.innerHTML += "Nombre2 es: " + nombre2 + "<br>"
	parrafoVariable.innerHTML += "nombre1+nombre2 es: " + 
	                         (nombre1+nombre2) + "<br>"
	parrafoVariable.innerHTML += "Variables de tipo fijo <br>";
	parrafoVariable.innerHTML += " Tipo Integer  <br>";
	parrafoVariable.innerHTML += "Numero1 es: " + numero1 + "<br>"
	parrafoVariable.innerHTML += "Numero2 es: " + numero2 + "<br>"
	parrafoVariable.innerHTML += "numero1+numero2 es: " + 
	                         (numero1+numero2) + "<br>"
}
function TiposVariables() 
{
	var parrafoVariable,
	    variable1 = "Ana Ruiz",
	    variable2 = "Juan Pérez",

    /* Con esta instrucción accedemos al párrafo de HTML
       cuyo ID es resultadoVariable. Una vez almacenamos este
       párrafo en la variable parrafoVariable podemos cambiar 
       cualquier de sus propiedades. Como por ejemplo, la
       propiedad innerHTML 
    */
	parrafoVariable = document.getElementById('resultadoVariable')
	parrafoVariable.innerHTML = "Variables de tipo variable <br>";
	parrafoVariable.innerHTML += " Tipo String <br>";
	parrafoVariable.innerHTML += "Variable1 es: " + variable1 + "<br>"
	parrafoVariable.innerHTML += "Variable2 es: " + variable2 + "<br>"
	parrafoVariable.innerHTML += "variable1+variable2 es: " + 
	                         (variable1+variable2) + "<br>"
	parrafoVariable.innerHTML += "variable1*variable2 es: " + 
	                         (variable1*variable2) + "<br>"
	variable1 = 124;         
	parrafoVariable.innerHTML += "Variables de tipo variable <br>";
	parrafoVariable.innerHTML += "Variable1 es: " + variable1 + "<br>"
	parrafoVariable.innerHTML += "Variable2 es: " + variable2 + "<br>"
	parrafoVariable.innerHTML += "variable1+variable2 es: " + 
	                         (variable1+variable2) + "<br>"
	parrafoVariable.innerHTML += "variable1*variable2 es: " + 
	                         (variable1*variable2) + "<br>"
}



function logico(){
	var a1 = true  && true;     // t && t devuelve true
var a2 = true  && false;    // t && f devuelve false
var a3 = false && true;     // f && t devuelve false
var a4 = false && (3 == 4); // f && f devuelve false
var a5 = "Cat" && "Dog";    // t && t devuelve "Dog"
var a6 = false && "Cat";    // f && t devuelve false
var a7 = "Cat" && false;    // t && f devuelve false
var o1 = true  || true;     // t || t devuelve true
var o2 = false || true;     // f || t devuelve true
var o3 = true  || false;    // t || f devuelve true
var o4 = false || (3 == 4); // f || f devuelve false
var o5 = "Cat" || "Dog";    // t || t devuelve "Cat"
var o6 = false || "Cat";    // f || t devuelve "Cat"
var o7 = "Cat" || false;    // t || f devuelve "Cat"
var n1 = !true;  // !t devuelve false
var n2 = !false; // !f devuelve true
var n3 = !"Cat"; // !t devuelve false
}