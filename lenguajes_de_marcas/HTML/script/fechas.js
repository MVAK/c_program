/*alumno : Mario Valle
teacher : Jose Luis Alvarez
enunciado: Web que lea una fecha y escriba el día siguiente a la fecha leída (teniendo en cuenta finales de mes y de año)
*/

function Fechas(){

var d1,
    m1,
    a1,
	resultado

	d1 = parseInt(document.getElementById('dia1').value)
	m1 = parseInt(document.getElementById('mes1').value)
	a1 = parseInt(document.getElementById('anio1').value)
	resultado = document.getElementById('resultado')

if ( (d1>0 && d1<32) || (m1 < 13 && m1 > 0) ){
	if ((d1==31) && ((m1==1)||(m1==3)||(m1==5)||(m1==7)||(m1==8)||(m1==10)||(m1==12) )) {/*
		para los meses que tienen 31 dias al aumentar el dia y cambian de mes */
		d1=1;
		resultado.innerHTML="la facha es"+ d1 + "/"+ (m1+1) + "/" + a1 ;
		if(m1==13){/* para cuando el mes es de 31 dias y ademas es diciembre pues los meses 
		vuelven a ser enero y los dias a uno y el año aumenta 1
		*/m1=1;
		resultado.innerHTML="la fecha es"+ d1 + "/"+ m1 + "/" + (a1+1);
		}
		
	}else
	    if (d1==30 && (m1==4||m1==6||m1==9||m1==11)) {
	   	/*para los meses que tienen 30 dias al aumentar el dia y cambian de mes */
		d1=1;
		resultado.innerHTML="la fecha es"+ d1 + "/"+ (m1+1) + "/" + a1 ;
		}else if (d1==28 && m1==2 && a1%4!=0){//para febrero que tiene 28 dias sin ser bisiesto
			d1=1;
		    resultado.innerHTML= "la fecha es "+ d1 + "/" + (m1+1) + "/" + a1;
		    }else if (d1==28 && m1==2 && a1%4==0){//para febrero que tiene 28 dias y es bisiesto
		    resultado.innerHTML= "la fecha es "+ (d1+1) + "/" + m1 + "/" + a1;
		    }


} 
else resultado.innerHTML= "Fecha no valida";// para cuando el intervalo de fechas no es valido
}