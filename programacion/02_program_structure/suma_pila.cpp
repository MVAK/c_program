

      ##############################################      ;
      ## Purpose: XXXXXX - Program Structure 1     
;##    In this program you can see: 
;## - Global and local varibles    
;## - c call convention     
;## - the constant nature of strings      
;## - the difference betweem variables and pointers: 
;##        BP vs [BP]      
;##############################################   
; ----------------------------------------
------------------------------------------------ 
; To assemble and run:     
;      
;     nasm -felf64 -l suma.lst -g suma.asm && ld suma.o -o suma && ./suma
; -------------------------------------------------------
---------------------------------  
10                             
11                                    
global _start  
12                                  
13                                  
14                               
_start:   
15 00000000 E827000000                      call    _main   
16                                 
17                                    ;
LLamar a la funcion exit del SO   
18 00000005 B83C000000                      mov     rax, 60  
19 0000000A 4831FF                          xor     rdi, rdi   
20 0000000D 0F05                            syscall  
21                                   
22                                    
23                                  function:    
24 0000000F 55                              push    rbp    
25 00000010 4889EC                          mov     rbp, rsp           
; Ya tenemos una pila nueva.  
26                                
27 00000013 4883EC10                        sub     rsp, 16           
; 3a. Que hace esto?    
28                                      
29 00000017 488B4504                        mov     rax, [rbp+4]        ; Meter en ax lo que haya en bp + 4
30 0000001B 488B5D08                        mov     rbx, [rbp+8]    
31 0000001F 4801D8                          add     rax, rbx               
; AX = AX + BX   
32 00000022 48894500                        mov     [rbp], rax  
33                                 
34 00000026 4883C410                        add     rsp, 16           
; 3b. Que hace esto?   
35 0000002A 5D                           pop     rbp             
; Restaurar la pila   
36 0000002B C3                              ret                        
; sacar la direccion de retorno 
37                                                                      
; de la pila y retornar    
38                                      39                                      40                                  _main:     41 0000002C 55                                  push     rbp                 ; main se hace su propia pila     42 0000002D 4889EC                              mov      rbp, rsp     43 00000030 4883EC40                            sub      rsp, 64     44                                      45                                      46                                              ; Llamada     47 00000034 6A03                                push     3                   ; vamos a hacer una llamada     48 00000036 6A06                                push     6                   ; los enteros ocupan 4 bytes     49 00000038 E8D2FFFFFF                          call     function     50 0000003D 4883C408                            add      rsp, 8              ; 1. Que hace esto?     51                                      52                                      53                                      54                                              ;add     rax, 0x61     55                                              ;mov      byte[mensaje + 17], al     56                                      57                                      58                                              ; Imprimir     59 00000041 B801000000                          mov     rax, 1               ; Llamada de sistema imprimir     60 00000046 BF01000000                          mov     rdi, 1               ; Fichero 1 (handle) = stdout     61 0000004B 48BE-                               mov     rsi, mensaje         ; Source - Dirección de donde leer     62 0000004D [6600000000000000]     63 00000055 BA12000000                          mov     rdx, len             ; Cantidad de bytes a imprimr     64 0000005A 0F05                                syscall     65                                              ; Fin de imprimir     66                                      67 0000005C 4883C440                            add     rsp, 64              ; 2a. Qué hace esto ?     68 00000060 5D                                  pop     rbp                ; Restaurar la pila del que me ha llamado.     69 00000061 C3                                  ret     70                                      71 00000062 00000000                resultado: db       0, 0, 0, 0          ; la b de db vien de byte     72 00000066 456C20726573756C74-     mensaje:   db       "El resultado es 0", 0x0A     73 0000006F 61646F20657320300A     74                                          len     equ $ - mensaje


