#include<stdio.h>
#include<unistd.h>
#define MAXCOLS 100
int main(){
	printf("hola \x0A \x09 beep \x0A");/*hexadecimal*/

	printf("hola \n \t beep \a \n");/*octal*/

        printf("hola \012 \011 beep \007 \012");/*codigos ascii*/

	/*printf(" hola \0x0A esto es secreto ");*/

	printf("\n");
        for (int vez=0; vez<=MAXCOLS;vez++){
		printf("\r");
		for (int igual=0;igual<vez;igual++)
			printf("=");
		printf("> %2i%%",vez);/* imprime dos caracteres que seran enteros*/
		fflush (stdout);
        usleep (100000);
    }
	printf(" cargado\n.FIN.");
	return 0;
}
