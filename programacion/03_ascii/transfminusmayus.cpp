#include<stdio.h>

#define SALTO ('a' - 'A')
/*
int main () {
	char letra = 'c';
	letra = letra - 0x20;
	printf("letra en mayusculas: %c\n" , letra);
	return 0;
}
*/
/*mejorado*/
int main(){
	char letra = 'c';
	/*si la letra es mayuscula*/
	if( letra >= 'a' && letra <='z')
	letra -= SALTO;    /*si pones letra-= te ahorras volver a escribir letra -
				 */
        else
	/*si no*/
	letra += SALTO;

	printf("letra en mayusculas: %c\n", letra);
	return 0;
}
