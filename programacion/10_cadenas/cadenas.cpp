#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#define MAXLIN 0x100

const char * const cprog = "cadenas";
char vprog[] = "programa";
void concatena(char *dest, const char *org, int max){

	while(*(dest++) != '\0')
		dest++;
	       	//Avanzardestino hasta el \0
		//copiar char a char de org a dest como mucho max o hasta \0
		for(int c=0; *org!='\0' && c<max; c++, ++org)
			*dest= *org;
	        *dest = *org;
}
void cifra (char *frase, int clave){
	while(*frase != '\0')
		*(frase++) += clave;
}
int main(int argc, char *argv[]){
	char linea[MAXLIN];   // array linea
	strcpy (linea, cprog);  // copy values of array (with the last character 0) to linea, but is a function unsafe
	strncpy ( linea, cprog, MAXLIN);  // function more safe because MAXLIN is a size of array of copy
	strcat (linea," ");
	strcat (linea, vprog, MAXLIN - strlen(linea));   // copy with no remove data in the file 
	concatena (linea,"Esto es un mensaje secreto", MAXLIN - strlen(linea));
	   cifra (linea + strlen(linea +1);
	printf( "%s\n", linea);
	printf ("Secreto: %s\n", linea + strlen(linea) + 1);
	return EXIT_SUCCESS;
}

