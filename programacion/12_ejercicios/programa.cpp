#include<stdlib.h>
#include<stdio.h>
//pedir numero al usuario
//
//
//
int main(){
    unsigned resultado;

    unsigned numero;
    printf("Dame un numero: ");
    scanf("%u", &numero);
    printf("Los divisores exactos de tu numero son: \n");
    //que números dividen exactamente ese número
    //si el numero es 25 que saquen todos los divisores de sus divisores

    for(int j=1; j<=numero; j++){
	printf("%u: ", j);
	for(int i=1; i<=j; i++)
	    if(j % i == 0)
		printf(" %u ", i );
	printf("\n");
    }

    return EXIT_SUCCESS;
}
