/* Ejercicio de la criba de eratostenes

Programmer: Mario Valle Armendariz
Teacher : Txema 

Enunciado : hacer la criba de eratostenes para un numero natural n

Definicion : La criba de Eratóstenes es un algoritmo que permite hallar todos los números primos menores que un número natural dado n. Se forma una tabla con todos los números naturales comprendidos entre 2 y n, y se van tachando los números que no son primos de la siguiente manera: Comenzando por el 2, se tachan todos sus múltiplos; comenzando de nuevo, cuando se encuentra un número entero que no ha sido tachado, ese número es declarado primo, y se procede a tachar todos sus múltiplos, así sucesivamente. El proceso termina cuando el cuadrado del siguiente número confirmado como primo es mayor que n. 
*/


#include <stdio.h>
#include <stdlib.h>

#define N 0x300

int main (int argc, char *argv[]) {
    unsigned primo[N];

    // Inicializacion 

    for (int i=0; i<N; i++)
        primo[i] = i+1;

    // Tachaje 

    for (unsigned i=1; i<N; i++)    // con la i vamos a ir tachando   
        if (primo[i]) //en la celda i
            for (unsigned j = 2 * primo[i] - 1; j<N; j+=primo[i])  // j es el numero que tachamos
                
            primo[j] = 0;



    //impresion de datos
    for (int k=0; k<N; k++)
        if (primo[k])
            printf ("%i ", primo[k]);

    printf ("\n");
    printf ("\n");

    return EXIT_SUCCESS;
}

