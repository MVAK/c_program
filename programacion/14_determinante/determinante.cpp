/*ejercicios epoca cuarentena
programmer: Mario Valle Armendariz
teacher: Txema

esta operativo y totalmente actualizado
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*
 Calcular el determinante de una matriz
  */
#define N 3

void ingresar_datos(double M[N][N],int d){
    printf("ingrese datos de la matriz:\n");
    for(int i=0;i<d;i++){
        printf("de la fila %i :",i+1);
        for(int j=0;j<d;j++){
            scanf("%lf",&M[i][j]);
        }
    }
}
 
double determinante(double M[N][N],int d){
    
    double aux;
    double determinante=1;
    for(int k=0;k<d-1;k++){//recorrer la diagonal
        determinante*=M[k][k];
        if(M[k][k]==0)
            return 0;
        else{
            
            for(int i=k+1;i<d;i++){//recorrer fila
                aux=-M[i][k];
                for(int j=k;j<d;j++){
                    M[i][j]=M[i][j]+aux*M[k][j]/M[k][k];
                }
              
            }
        }
      
    }
    determinante*=M[d-1][d-1];
    return determinante;// devolvemos el valor del determinante
}


int main (int argc, char *argv[]) {
    double M[N][N];
    int d;//le he puesto lo de la dimension pero no hace falta porque esta definida para ser de rango 3 
    printf("ingrese dimension de la matriz: ");
    scanf("%d",&d);
    ingresar_datos(M,d);
    printf("el determinante es: %lf \n", determinante(M,d));
    return EXIT_SUCCESS;
} 
