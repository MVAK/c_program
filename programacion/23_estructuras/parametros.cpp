#include <stdio.h>
#include <stdlib.h>

struct TParDiv {
    int dividendo;
    int divisor;
};

int dividir (void *parametros) {
    struct TParDiv p = * (struct TParDiv *) parametros;
    return p.dividendo / p.divisor;
}

int halving (void *parametros) {
    int *par = (int *) parametros;
    int p = *par;
    return p >> 1;
}

int main (int argc, char *argv[]) {

    int (*p) (void *);
    int dividendo  = 8,
        divisor    = 2;
    struct TParDiv par_div;

    p = &dividir;
    par_div.dividendo = dividendo;
    par_div.divisor   = divisor;

    printf (" dividir (%i,%i) = %i\n", dividendo, divisor, (*p) ((void *) &par_div));
    p = &halving;
    printf (" halving (%i) = %i\n", dividendo, (*p) ((void *) &dividendo));


    printf ("\n");

    return EXIT_SUCCESS;
}
