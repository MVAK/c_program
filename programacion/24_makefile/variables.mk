A = 3
B = $(A)
C := $(A)
A = 4

.PHONY: info

info:
	$(info B=$(B) y C=$(C))
