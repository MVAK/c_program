#include <stdlib.h>
#include <time.h>
#include <errno.h>

#include "stack.h"
#include "interface.h"

int main (int argc, char *argv[]) {
    struct TStack stack;

    init (&stack);

    srand (time (NULL));
    for (int i=0; i<7; i++) {
        push (&stack, rand () % 10 - 3);
        if (errno == ENOMEM)
            show_error ("Stack push operation failed.");
    }

    show_stack (stack);

    printf ("\t=> %i\n", pop (&stack));
    if (stack.failed)
        show_error ("Stack pop operation failed.");

    printf ("Pulse una tecla para continuar.");
    getchar ();

    show_stack (stack);

    destroy (&stack);
    return EXIT_SUCCESS;
}
