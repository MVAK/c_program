#include <stdlib.h>
#include <time.h>

#include "queue.h"
#include "interface.h"

int main (int argc, char *argv[]) {
    struct TQueue queue;
    int lim = M < 5 ? M : 5;

    init (&queue);

    srand (time (NULL));

    for (int i=0; i<lim; i++) {
        push (&queue, rand () % 10 - 3);
        if (queue.failed)
            show_error ("Stack push operation failed.");
    }

    show_queue (queue);

    printf ("\t=> %i\n", shift (&queue));
    if (queue.failed)
        show_error ("Stack pop operation failed.");

    printf ("Pulse una tecla para continuar.");
    getchar ();

    show_queue (queue);

    printf ("Pulse una tecla para continuar.");
    getchar ();

    push (&queue, 7);
    show_queue (queue);

    printf ("Pulse una tecla para continuar.");
    getchar ();

    return EXIT_SUCCESS;
}
