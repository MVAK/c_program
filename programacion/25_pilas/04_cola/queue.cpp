#include "queue.h"

/* FUNCIONES DE LA MÁQUINA DE DATOS */
void init (struct TQueue *s) {
    s->head   = 0;
    s->summit = 0;
    s->failed = 0;
}

void push (struct TQueue *s, int ndata) {
    s->failed = 0;
    if (s->summit >= M) {
        s->failed = 1;
        return;
    }
    s->data[s->summit++] = ndata;
}

int shift (struct TQueue *s) {
    s->failed = 0;
    if (s->head >= s->summit) {
        s->failed = 1;
        return -666;
    }
    return s->data[s->head++];
}
