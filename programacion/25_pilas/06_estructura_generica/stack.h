#ifndef __STACK_H__
#define __STACK_H__

#define M 0x05

typedef double custom_t;

struct TStack {
    custom_t data[M];
    int summit;
    int failed;
};

#ifdef __cplusplus
extern "C"
{
#endif
    void init (struct TStack *s);
    void push (struct TStack *s, custom_t ndata);
    custom_t pop (struct TStack *s);
#ifdef __cplusplus
}
#endif

#endif
