# Proyecto Canon

Analizamos esta partitura en sol mayor.

![Partitura fuente](https://i.pinimg.com/originals/f8/94/b4/f894b467576213d93e51d00946a1aca9.jpg)

## Notas

### Do Central del piano (DO4)

```
00: Do
01: Do#
02: Re
03: Re#
04: Mi
05: Fa
06: Fa#
07: Sol
08: Sol#
09: La
10: La#
11: Si
```
A partir de aquí se repiten, pero una octava más arriba.

### DO5
```
12: Do
13: Do#
14: Re
15: Re#
16: Mi
17: Fa
18: Fa#
19: Sol
20: Sol#
21: La
22: La#
23: Si
```


### Duraciones

A continuación se muestra el id de cada figura, la duración en fracciones de compás y su nombre.

| id: | Fracción | Nombre |
|----:|:--------:|--------|
| 0 | 1 | redonda |
| 1 | 2 | blanca  |
| 2 | 4 | negra |
| 3 | 8 | corchea |
| 4 | 16 | semicorchea |


## Partitura.

Como es una partitura en sol mayor, todos los fa y do
son sostenidos (#) salvo que se indique lo contrario.

sol aparece como sl.  
Si la nota termina en mayúsculas se considera que es con puntillo.  
`-`: indica silencio.



### Formato Transcripción

Compás inicial, duración(id) y notas
```
00: 1 ( re2  la   si   fa   sl   re   sl   la  )
04: 1 ( fa2  mi2  re2  do2  si   la   si   do2 )
08: 1 ( re2  do2  si   la   sl   fa   sl   mi  )
12: 2 ( re   fa   la   sl   fa   re   fa   mi  )
14: 2 ( re   si   re   la   sl   si   la   sl  )
16: 2 ( fa   re   mi   do2  re2  fa2  la2  la  )
18: 2 ( si   sl   la   fa   re   re2  rE  3do  )
20: 3 ( re2  do2  re2  re   do   la   mi   fa  )
21: 3 ( do   re2  do   si   do2  fa2  la2  si2 )
22: 3 ( sl2  fa2  mi2  sl2  fa2  mi2  re2  do2 )
23: 3 ( si   la   sl   fa   mi   sl   fa   mi  )
24: 3 ( re   mi   fa   sl   la   mi   la   sl  )
25: 3 ( fa   si   la   sl   la   sl   fa   mi  )
26: 3 ( re   si   si   do2  re2  do2  si   la  )
27: 3 ( sl   fa   mi   si   la   si   la   sl  )
28: 2 ( fa   fa2 1mi )
29: 1 (2-   2re2  fa2  si   la   si   do2 1re2 1re  do )
33: 1 (2-   2si   re2  rE2 2re2 )
35: 2 ( re2  sl2  mi2  la2 )
```

## Ejecución

``` bash
./videal <fichero.score>
```
