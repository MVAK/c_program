#ifndef __SCORE_PARSER_H__
#define __SCORE_PARSER_H__

#ifdef __cplusplus
extern "C"
{
#endif

    struct TScore *parse_file (const char *filename);

#ifdef __cplusplus
}
#endif

#endif
