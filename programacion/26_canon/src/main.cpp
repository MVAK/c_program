#include <stdlib.h>

#include "usage.h"
#include "score_parser.h"
#include "score.h"
#include "wave.h"

#define FREQ 44100

int main (int argc, char *argv[]) {
    struct TScore *sc = NULL;
    struct TWav *wav  = NULL;

    /* Initialization */
    usage_init (argv[0]);
    if (argc<2)
        print_usage (stderr);



    /* Load data */
    sc = parse_file (argv[1]);
    wav = render_score (sc, 0);


    /* Output */
    print_sc(sc);
    play (wav, 0);


    /* Finalization */
    destroy_wav (wav);
    destroy_score (sc);

    return EXIT_SUCCESS;
}
