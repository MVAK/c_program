#include "scales.h"

#include <string.h>
#include <ctype.h>

const char *sharp = "♯";
const char *flat  = "♭";

/* Fifths circle */
const char * const scales[2][12] = {
    { "FaM", "DoM", "SolM", "ReM", "LaM", "MiM", "SiM", "SolbM", "RebM", "LabM", "Mib", "SibM" },
    { "Rem", "Lam", "Mim", "Sim", "Fa#m", "do#m", "Sol#m", "Mib", "SibM", "Fam" "Dom", "Solm" }
};

const char * const note_names[]  = { "-",  "do" , "re", "mi", "fa" , "sl", "la", "Si" };
const unsigned note_tones[] = {0, 2, 4, 5, 7, 9, 11};

const char * const outputnames[2][12][8] = {
    /* Major scale */
    {
        { "𝄽", "Do" , "Re" , "Mi" , "Fa" , "Sol" , "La" , "Si♭" },   // For F  major
        { "𝄽note_names", "Do" , "Re" , "Mi" , "Fa" , "Sol" , "La" , "Si"  },   // For C  major
        { "𝄽", "Do" , "Re" , "Mi" , "Fa♯", "Sol" , "La" , "Si"  },   // For G  major
        { "𝄽", "Do♯", "Re" , "Mi" , "Fa♯", "Sol" , "La" , "Si"  },   // For D  major
        { "𝄽", "Do♯", "Re" , "Mi" , "Fa♯", "Sol♯", "La" , "Si"  },   // For A  major
        { "𝄽", "Do♯", "Re♯", "Mi" , "Fa♯", "Sol♯", "La" , "Si"  },   // For E  major
        { "𝄽", "Do♯", "Re♯", "Mi" , "Fa♯", "Sol♯", "La♯", "Si"  },   // For B  major
        { "𝄽", "Do♭", "Re♭", "Mi♭", "Fa" , "Sol♭", "La♭", "Si♭" },   // For G♭ major
        { "𝄽", "Do" , "Re♭", "Mi♭", "Fa" , "Sol♭", "La♭", "Si♭" },   // For D♭ major
        { "𝄽", "Do" , "Re♭", "Mi♭", "Fa" , "Sol" , "La♭", "Si♭" },   // For A♭ major
        { "𝄽", "Do" , "Re" , "Mi♭", "Fa" , "Sol" , "La♭", "Si♭" },   // For E♭ major
        { "𝄽", "Do" , "Re" , "Mi♭", "Fa" , "Sol" , "La" , "Si♭" }    // For B♭ major
    },
    /* Minor scale */
    {

    }
};

const unsigned name_len = sizeof (note_names) / sizeof (char *);

const char *get_scname (const struct TScale *sc) {
    return scales[sc->mode][(int) sc->nn_idx];
}

// Returns idx containing de name of the given note.
enum TScName get_scidx(const char *name, enum TScMode m) {

    unsigned i;
    enum TScName current = TScName::Do;  // Do default scale

    for (i=0; i<8; i++)
        if ( (strcasecmp ((const char *) scales[(int) m][i], name) ) == 0 )
                return (enum TScName) i;

    return current;
}

struct TScale create_scale () {
    struct TScale s;

    s.mode = major;
    s.nn_idx = TScName::Do;

    return s;
}


/* Returns the number of the equivalent piano key
 *
 * Return value is of type int beacause GbM has do flat which is a value of -1
 **/
int note_value (struct TScale sc, enum TNoteName note) {
    int inc = 0;
    unsigned base = note_tones[(int) note-1];
    const char *scale_name = (const char *) outputnames[(int) sc.mode][(int) sc.nn_idx][(int) note];
    int len;

    // Increment idx to find possible sharps and flats (unicode):
    for (len = 0; scale_name[len] != 0 && scale_name[len] > 0 /*unicode character. Less than 0x80 if unsigned char */; len++);

    /* Check if scale name ends in sharp of flat to modify tone value */
    if ( (strcmp(sharp, &scale_name[len])) == 0 )
        inc =  1;
    if ( (strcmp(flat , &scale_name[len])) == 0 )
        inc = -1;

    return base + inc;
}


// Makes an structure to represent the string scale found in file
struct TScale process_scale (char *txt) {
    struct TScale sc;
    char note[4];
    enum TScMode mode = major;
    int len = strlen(txt);
    int mode_specified = 0;

    if (txt[len-1] == 'm')
        mode = minor;
    sc.mode = mode;
    sc.nn_idx = get_scidx (txt, mode);

    /* Decided to leave mode on scale name */
    // /* Rip mode. Already got it. */
    // if (tolower (txt[len-1]) == 'm')
    //     txt[len-1] = '\0';


    return sc;
}
