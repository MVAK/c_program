#include "ccompuesta.h"

CCompuesta::CCompuesta ()
{
    this->compuesto = new CCompuesto ();
}

CCompuesta::CCompuesta (CCompuesto el) : el2(el)
{
    CCompuesto x = el;  // Copia todos los valores de memo
    CCompuesto y(el);   // Constructor de copia
    CCompuesto z = CCompuesto(el); // Copia el attr en z

    this->el2 = CCompuesto (el);
}

CCompuesta::CCompuesta (const CCompuesta &ref) {
    this->compuesto = new CCompuesto ();
    this->el2 = ref.el2;
}
CCompuesta::~CCompuesta ()
{
    delete this->compuesto;
}

