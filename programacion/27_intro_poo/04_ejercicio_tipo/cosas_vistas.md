# Cosas vistas

1. Declaración de clases.
1. Implementación de métodos.
1. Constructor no puede ser virtual.
1. Constructores protected.
1. Inicializadores.
1. Valores por defecto en los parámetros.
1. Llamadas implicitas al constructor de la clase base.
1. Llamadas explicitas al constructor de la clase base.
1. Sobrecarga de métodos.
1. Herencia.
1. Los destructores se llaman en cascada para terminar con los atributos private de la clase padre.
1. Accedente y mutadores.
1. Variables sólo lectura, sólo escritura, lectura y escritura.
1. Variables virtuales.
