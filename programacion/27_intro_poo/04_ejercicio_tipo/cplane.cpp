#include "cplane.h"
#include "cvehicle.h"

#include <iostream>

CPlane::CPlane ()
{
}

CPlane::CPlane (
            double velMax,
            double fuelQ,
            VelUnit unitVel,
            FuelType tFuel
           ): CVehicle( velMax, fuelQ, unitVel, tFuel)
{
}


void
CPlane::move () {
    std::cout << "Me he movido" << std::endl;
}

CPlane::~CPlane () {
    std::cout << "Se ha ejecutado el destructor de CPlane." << std::endl;
}
