#include "cvehicle.h"

#include <iostream>

using namespace std;

CVehicle::CVehicle ()
{
    cout << "Se ha ejecutado el constructor de CVehicle." << endl;
}

CVehicle::CVehicle (
        double velMax,
        double fuelQ,
        VelUnit unitVel,
        FuelType tFuel
        )
    : vmax(velMax), qFuel(fuelQ), velUnit(unitVel), fuelType(tFuel)
{
    cout << "Se ha ejecutado el constructor de CVehicle." << endl;
}

CVehicle::~CVehicle () {
    cout << "Se ha ejecutado el destructor de CVehicle." << endl;
}
