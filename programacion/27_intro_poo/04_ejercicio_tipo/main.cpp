#include <stdio.h>
#include <stdlib.h>

#include "cvehicle.h"
#include "cplane.h"

void do_something () {
    CPlane plane = CPlane(200, 70, VelUnit::mph, FuelType::AvGas);

    plane.move();
}

int main (int argc, char *argv[]) {
    do_something ();
    return EXIT_SUCCESS;
}
