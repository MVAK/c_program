/*************************************
 * Programmer : Mario Valle Armendariz
 * Teacher : Txema
 *
 * Enunciated: make colours with rgb
 * ***********************************/

#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]){
	unsigned char r, g, b;
        unsigned int entrada;
	unsigned char xr;

        printf("intensidad de rojo");
        scanf("%i", &entrada);
        r = (unsigned char) entrada;//revalue to 1 bit


	 printf("intensidad de verde");
        scanf("%i", &entrada);
        g = (unsigned char) entrada;


        printf("intensidad de azul");
        scanf("%i", &entrada);
        b = (unsigned char) entrada;
        


	xr = r ^ 0xFF ;  //255
	printf("#%X%X%X => #%X%X%X\n" , r, g, b, xr, g, b);//In Hexadecimal

	return EXIT_SUCCESS;
}
