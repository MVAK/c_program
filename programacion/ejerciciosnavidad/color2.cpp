
/*************************************
 * Programador : Mario Valle Armendariz
 * Profesor : Txema
 *
 * Enunciado: hacer los colores con rgb mejorado con funciones
 * ***********************************/

#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>

void titulo (){
	system("clear");
	system("toilet --gay -fpagga COLOR");
	printf("\n\n");
}
unsigned char pregunta_intensidad(const char *componente){ // es una funcion que coge la direccion de la cadena de caracteres y busca donde esta para utilizar sus valores
	unsigned int entrada; 
	do {
	     __fpurge (stdin); //antes de iniciar vacia el tubo, en caso que responda mal vuelve a preguntar lo mismo, si responde bien sigue adelante
	     printf("intensidad de %s [0-255] : ", componente); 
             scanf("%u", &entrada);//la u es de unsigned
	}while (entrada > 0xFF);
        return (unsigned char) entrada;//para que sea de 1 bit
}
int main (int argc, char *argv[]){
	
	unsigned char r, g, b;
	unsigned char xr;
	
	titulo ();
        r = pregunta_intensidad ("rojo");// llamas a la funcion con rojo para hallar la intensidad de rojo en este caso y asi con los otros casos
        g = pregunta_intensidad ("verde");
	b= pregunta_intensidad ("azul");
        


	xr = r ^ 0xFF ;  //255
	printf("#%X%X%X => #%X%X%X\n" , r, g, b, xr, g, b);//En Hexadecimal

	return EXIT_SUCCESS;
}
