/**************************************************************
 * Programador: Mario Valle Armendariz
 * Profesor: Txema
 * Enunciado: Imprime nombre x
 * ***********************************************************/


#include<stdio.h>
#include<stdlib.h>

int main () {

    char nombre[largo];     /* Usamos un array para que pueda
                           * guardar los caracteres del nombre.
                           * El tamaño del array
                           * tiene que ser lo suficientemente grande
                           * para que el scanf que hay
                           * más abajo pueda guardar los valores
			   * Si no, daría un error:
                           * stack smashing detected  */
    int veces;

    printf ("Indica el nombre que quieres imprimir: \n");
    scanf (" %[^\n]s", nombre);   /* Esta opción de scanf nos
                       * permite recoger los datos, evitando la
                       * limitación con que cuenta scanf. Esta es,
                       * en el momento que haya un espacio, dejará
                       * de recoger datos, al considerarlo como un
                       * salto de linea.
                       * Solo dejará de recoger datos tras un
                       * salto de línea por lo que terminara el programa.*/

    printf ("Indica cuántas veces quieres imprimir el nombre: \n");
    scanf (" %i", &veces);

    for (int contador = 0; contador < veces; contador++)
        printf ("%i El nombre que quieres imprimir es %s.\n", contador+1, nombre);

    return EXIT_SUCCESS;
}
