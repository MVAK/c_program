/*****************************************************************
 * Programador: Mario Valle Armendariz
 * profesor: Txema
 * enunciado: imprimir el nombre
 * ***************************************************************/

#include <stdio.h>
#include <stdlib.h>

int main () {

    for (int contador = 0; contador < 10; contador ++)
        printf ("%i Mi nombre es Mario Valle Armendariz.\n", contador + 1);

    return EXIT_SUCCESS;
}
