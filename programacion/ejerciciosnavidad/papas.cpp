#include <stdio.h>
#include <stdlib.h>

#define PAPUP 8 //1000
#define MAMUP 4 //0100
#define HIJOP 2 //0010
#define HIJAP 1 //0001

// waked = PAPUP | HIJAP; //
// waked = waked | PAPUP; //Poner al padre despierto si o si
// waked = waked ^ PAPUP; //Conmutar el padre, si esta dormido que se despierte y si esta ddespierto que se duerma
// waked = waked & ~PAPUP; //Acostar al padre

enum TMiembro {
    papa,
    mama,
    hijo,
    hija,
    total_miembros
};


/* conmutar: devuelve el valor old_status habiéndo cambiado el valor del bit correspondiente
al miembro de la familia */
int conmutar (int old_status, enum TMiembro miembro) {
int new_status;
int bit;
    switch (miembro) {
	    case papa:
		    bit = PAPUP;
        break;
            case mama:
		    bit = MAMUP;
        break;
	    case hijo:
	            bit = HIJOP;
	break;
	    case hija:
	            bit = HIJAP;
	break;
	    default:
	bit = 0;
    }
	  new_status = old_status ^ PAPUP;
    return new_status;
}

/* acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente
al miembro de la familia */
int acostar (int old_status, enum TMiembro miembro) {
	int bit = 1,
	    pos = total_miembros - 1 - miembro; // invierto los miembros de la familia para que tenga a todos en cuenta por si nace un futuro hijo o algo parecido
	for (int vez=0; vez<pos;vez++) // bit => PAPUP | MAMUP ... 
		bit = bit << 1;

    return old_status & ~bit;
    }


/* levantar: devuelve el valor old_status( que es el numero de familiares que habia levantados en octa) habiéndo levantado(con el valor de enum) el bit correspondiente
al miembro de la familia */
int levantar (int old_status, enum TMiembro levantado) {
 int bit = 1,
            pos = total_miembros - 1 - levantado; // invierto los miembros de la familia para que tenga a todos en cuenta por si nace un futuro hijo o algo parecido
        for (int vez=0; vez<pos;vez++) // bit => PAPUP | MAMUP ... 
                bit = bit << 1;

    return old_status | bit;/*int pos= total_miembros - 1 - levantado;
			      return old_status | (int) pow (2,pos)         esta es otra forma*/
    }


/* Imprime con texto el estado de cada miembro de la familia*/
void print (int waked) {
    printf ("Papá está %s\n",waked & PAPUP ?  "levantado": "acostado");
}

int main () {
    int waked = 0;
    enum TMiembro despertado = papa;
    
    waked = levantar (waked, papa);
    waked = acostar (waked, papa);
    waked = conmutar (waked, papa);
    
    print (waked);
    
    return EXIT_SUCCESS;
}

