/*****************************************************
 * Programador: Mario Valle Armendariz
 * profesor : Txema
 * Enunciado: hallar el resto de una division entera
 * ***************************************************/

#include <stdio.h>
#include <stdlib.h>

int main () {

    double dividendo = 13.0;

    int divisor = 7;
    double resto;
    resto = (int)dividendo % divisor;
    printf ("Vamos a realizar la división de:\n\
            un número double, como el número 13.0,\n\
            con un entero como el número 7.\n\
            Esto lo vamos a realizar mediante un casting o molde:\n\
            13.0 / 7 = %.2lf\n", dividendo / (double)divisor);
    printf ("Ahora vamos a mostrar el resto de la division: %.2lf\n", resto);
    return EXIT_SUCCESS;
}
