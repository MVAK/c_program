
/**********************************************************************************
 * Programmer: Mario Valle Armendariz
 *
 * Teacher: Txema
 *
 * Statement: If a color is defined by unsigned char rgb [3]; And we have to:
#define R 2
#define G 1
#define B 0
If we define a mask unsigned char mask [3]; :
Are you able to:
1. Ask the user for a color and a mask.
2. Make an XOR of the color with the mask.
3. Leave an example of the two colors made with some drawing program.
***********************************************************************************/

/************************************************************* Libraries ******************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdio_ext.h>
#include <string.h>

#define R 2
#define G 1
#define B 0
#define COLS 3                                                           //capacity of arrays

const char *const colores[] = {               // colores is a array that have a direction of memory of the first cell, and each cell have direction of every colour
	"Azul",
	"Verde",
	"Rojo"
};


void titulo (){                                                          //make a title
	system("clear");
	system ("toilet --gay -fpagga COLORES");
}
/************************************************************ Functions ********************************************************************/


void imprimir_color (const char *etiqueta, unsigned char color[COLS]){   // print colour
	printf ("%s ", etiqueta);
	printf(" #");
	 for (int col=B; col<COLS; col++)
		 printf ("%2X", color[col]);                             // print in hexadecimal 2
	 printf ("\n");
}


unsigned char pedir_color (const char *nombre){  	                 // function to call at int main
        int letra;
	int buffer;
 
	printf("\n");

        do {

               printf("\x1B[1A");                                        // call to beginning if answer fail
               printf("%s (0-255): ",nombre);                            // show colour that you are asking 
	       printf(" ");                                // space to write answer
	       printf ("\x1B[1D"); 	                                 //delete last answer if it is fail
	       letra = getchar ();                                       //see tube and chek another letters or number that are not correct
	       if (letra < '0' || letra > '9')
	       __fpurge(stdin);     	                                 // flush
	       else 
		       ungetc (letra, stdin);
               scanf (" %u", &buffer);                                   // save into buffer the answer of color intensity
	
        }while (buffer < 0 || buffer >255);                              //because the rank of each color is between 0-255
return (unsigned char) buffer;                                           //for change to 1 byte because before value 4 bytes and now its not necessary so much value
}


void input (unsigned char data[COLS], const char *etiqueta){

	int longitud = strlen(etiqueta);
	printf("\n");
	printf("%s:\n", etiqueta);
	for(int s=0; s<=longitud; s++)
        printf ("=");
    printf ("\n");

	printf ("=======\n");
	for (int color=B; color<COLS; color++)
		data[color] = pedir_color(colores[color]);
}

/*******************************************************************************************************************************************
********************************************************** main function******************************************************************/
int main (int argc, char *argv[]){
        unsigned char rgb[COLS],                                         // capacity arrays of colours
        mask[COLS],
        resultado[COLS];
	titulo();
/********************************************************** Data entry *********************************************************************/


        input (rgb, "COLOR");
	input (mask, "MASCARA");


/********************************************************** Calculations *******************************************************************/


        for (int col=B; col<COLS; col++)                                 // make XOR of color with the mask
        resultado[col] = rgb[col] ^ mask[col];


/********************************************************** Data output ********************************************************************/


	 imprimir_color("Original: ", rgb);
	 imprimir_color("Mascara: ",  mask);
	 imprimir_color("Resultado: ", resultado);
	



   return EXIT_SUCCESS;
}
