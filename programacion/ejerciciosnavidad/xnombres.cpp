/**********************************************************
 * Programador: Mario Valle Armendariz
 * profesor: Txema
 * enunciado: imprime x nombre n veces
 * ********************************************************/


#include<sdtio.h>
#include<stdlib.h>



int main () {

    char nombre[largo];
    int veces;

    printf ("Introduzca el nombre que quiere imprimir: \n");
    fgets (nombre, LONGITUD, stdin);    /* fgets es una
                * "variación" de scanf, que permite recoger
                * todos los caracteres que le lleguen.
                * Se utiliza como:
                * (nombre_variable, tamaño_max_carac, entrada)
                * pudiendo ser la entrada tanto como el teclado
                * -stdin- o un fichero.  */

    printf ("¿Cuántas veces quiere imprimirlo?: \n");
    scanf (" %i", &veces);

    for (int contador = 0; contador < veces; contador++)
        printf ("%i El nombre que quiere imprimir es %s", contador+1, nombre);

    return EXIT_SUCCESS;
}
