#include<stdlib.h>
#include<stdio.h>
#include<string.h>


//**************** Data Entry *****************
#define ALR 20

int main(int argc, char *argv[]){
	unsigned char l[ALR];  //20 numbers 0 to 255

//**************** Calculations *****************

	for (int i=0; i<ALR; i++) 
		l[i] = (i + 1) * (i + 1);

//**************** Data Print *****************

	for (int j=0; j<ALR; j++)
		printf("%i\n", l[j]);


	return EXIT_SUCCESS;
}
