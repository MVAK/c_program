#include <stdio.h>
#include <stdlib.h>

#define N 6

void muestra (int *a[N]) {
    for (int i=0; i<N; i++)
        printf ("%i ", *a[i]);
    printf ("\n");
}

int main (int argc, char *argv[]) {
    int A[N] = {2, 5, 3, 7, 4, 1};
    int *o[N];

    for (int i=0; i<N; i++)
        o[i] = &A[i];

    muestra (o);
    /* Ordenación de la burbuja (bubble sort) */
    for (int p=0; p<N-1; p++)
        for (int i=0; i<N-1; i++)
            if (*o[i] < *o[i+1]) {
                int *aux = o[i+1];
                o[i+1] = o[i];
                o[i] = aux;
            }
    /* Fin de la ordenación */
    muestra (o);


    return EXIT_SUCCESS;
}

