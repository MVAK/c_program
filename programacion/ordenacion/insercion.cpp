#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define N 6
#define M 20

/* Estudia este código en paralelo con el del día 17 de marzo */

void muestra (int *a[N] /* Antes el parámetro era int a[N] */) {
    for (int i=0; i<N; i++)
        printf ("%i ", *a[i]);
    printf ("\n");
}

int main (int argc, char *argv[]) {
    int A[N];
    int *o[N]; // Punteros ordenados;   *** NUEVO ***
    int menor;

    /* Inicialización */
    srand (time (NULL));
    for (int i=0; i<N; i++)
        A[i] = rand () % M + 1;   // Números entre 1 y 20

    /*** NUEVO  ***/
    for (int i=0; i<N; i++)  // Cada celda de o apunta ordenadamente a una celda de A:
        // o[i] = A + i;  // Lo de abajo es equivalente
        o[i] = &A[i];

    muestra (o);
    /* Ordenación de inserción */

    for (int buscando=0; buscando<N-1; buscando++) {
        menor = buscando;
        for (int i=buscando+1; i<N; i++)
            if (*o[i] < *o[menor])
                menor = i;
        if (menor > buscando) {
            int *aux = o[buscando];
            o[buscando] = o[menor];
            o[menor] = aux;
        }
    }
    /* Fin de la ordenación */
    muestra (o);


    return EXIT_SUCCESS;
}

