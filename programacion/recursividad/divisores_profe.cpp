#include <stdio.h>
#include <stdlib.h>

void divisores (
        int n_original, /* Numero original */
        int n_divisor   /* Número posible divisor */)
{
    if (n_divisor < 1)  /* COndición de salida */
        return;

    if (n_original % n_divisor == 0)
        printf ("%i ", n_divisor);

    divisores (n_original, n_divisor - 1);

}

int main (int argc, char *argv[]) {

    int n = 91;

    printf ("Divisores de %i => ", n);
    divisores (n, n/2);  // El divisor posible más grande de n es n/2

    printf ("\n");

    return EXIT_SUCCESS;
}

