#include <stdio.h>
#include <stdlib.h>


int suma (int n){
    return n + (n <= 0 ? 0: suma (n-1) ) ;

}

int main (int argc, char *argv[]) {

    int n;
    printf("Hasta que numero quieres sumar (sumara todos los numeros naturales hasta el que pida inclusive): ");
    scanf("%i",&n);
    printf ("suma (%i) = %i\n", n, suma (n));

    return EXIT_SUCCESS;
}

