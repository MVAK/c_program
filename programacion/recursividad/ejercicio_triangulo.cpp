/*programmer: Mario Valle Armendariz
Teacher: Txema
Exercise: imprime area de triangulo o rectangulo que a veces apunte a uno y a veces a otro;
 */



#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>




double area_triangulo(double lado, double otro_lado) {

	double area, altura;

	altura = sqrt(pow(lado,2)-pow(lado/2,2));

	printf("el area del triangulo es: %lf", area=(lado*altura)/2);

	return area;
}


double area_rectangulo ( double lado, double otro_lado){
	double area;
	printf("el area del rectangulo es : %lf ", area=lado*otro_lado);

	return area;
}


int main (int argc, char *argv[]){

	double lado, otro_lado;
	char seleccion[0];

	printf("De que quieres hallar el area: triangulo o rectangulo? \n");
	scanf("%s", seleccion);

	if (strcmp(seleccion, "triangulo") == 0){
		printf("has elegido %s \n", seleccion);
		printf("cuanto mide el lado mas grande del triangulo? : ");
		scanf("%lf", &lado);
		printf("y el otro lado?: ");
		scanf("%lf", &otro_lado);
		printf("\n El area del triangulo es: %d", area_triangulo(lado,otro_lado));
	}
	else if ( strcmp(seleccion, "rectangulo") == 0){
		printf("Has elegido %s \n", seleccion);
		printf("Cuanto mide un lado del rectangulo? : ");
		scanf("%lf", &lado);
		printf(" Y el otro lado?: ");
		scanf("%lf", &otro_lado);
		printf("\n El area del triangulo es: %lf", area_rectangulo(lado,otro_lado));

	}
	else printf("Esa no es ninguna de las opciones, piensa un poco...");

	return EXIT_SUCCESS;
}


