/*Programmer: Mario Valle Armendariz
Teacher : Txema
Program: select ERROR to search what interval of e are posible

*/

#include <stdio.h>
#include <stdlib.h>


int factorial (int n) {
    if ( n == 0 )
        return 1;

    return n * factorial (n-1);
}

double e (unsigned n, double error) {  
                         
    double k = 1. / factorial (n);
    if ( k < error ) // k= difference with the last e , if k is more little than error , print k , if not print k + recursive function e +1 , until see the little error between two e
        return k;    
    return k + e(n+1,error);
}


int main (int argc, char *argv[]) {
    double err;
    printf("que error quieres que haya: ");
    scanf("%lf",&err);
    printf ("e = %lf\n", e (0,err));  // we begin at 0 with the error that we need of the user, that print the last e(n) that have the minimun of error  

    return EXIT_SUCCESS;
}

