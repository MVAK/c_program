#include <stdio.h>
#include <stdlib.h>

int factorial (int n){
    if (n <= 0)
        return 1;
    return n * factorial (n-1);

}

int main (int argc, char *argv[]) {

    int n;

    printf("De que numero quieres hacer el factorial: ");
    scanf("%i",&n);

    printf ("factorial (%i) = %i\n", n, factorial (n));

    return EXIT_SUCCESS;
}
