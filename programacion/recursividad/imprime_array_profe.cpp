#include <stdio.h>
#include <stdlib.h>

void imprime (const char *letra) {
    if (*letra != '\0') {
        printf ("%c", *letra);
        imprime (letra + 1);
    }
}

int main (int argc, char *argv[]) {
    char frase[] = "dabale arroz a la zorra el abad";

    imprime (frase);

    printf ("\n");


    return EXIT_SUCCESS;
}

