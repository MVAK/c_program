
/*programmer: Mario Valle Armendariz
Teacher: Txema
Exercise: hacer integral de funcion y dejar elegir al usuario el limite inferior y el superior
 */



#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#define MAX_ERROR .000001
double funcion (double *polinomio, double grado, double x) {

	double altura = 0,
		   potencia = 1;

	for (int p=0; p<grado; p++, potencia*=x)
		altura += polinomio[p] * potencia;

	return altura;
}


double integral (double *polinomio, int d, int li, int ls, double incremento) {
	double suma = 0;
	for (double x=li; x<ls; x+=incremento)
		suma += funcion(polinomio, d, x);
	suma *= incremento;

	return suma;
}
double *pedir_datos (int *dim) {
	double buffer, *polinomio = NULL;
	char fin[2];

	*dim = 0;
	printf ("Introduce el Polinomio\n");
	printf ("ej: 3x² + 2x + 1.5 => (1.5 2 3)\t Polinomio: ");
	scanf (" %*[(]");

	scanf (" %*[(]");

	do {
		polinomio = (double *) realloc(polinomio, (*dim+1) * sizeof(double));
		scanf(" %lf", &buffer);
		polinomio[(*dim)++] = buffer;
	} while (!scanf(" %1[)]", fin));

	return polinomio;
}



int main (int argc, char *argv[]){
	double *pol, li, ls;   
	int dim;                             
	double area, ultima_area;


	printf ("\n");
	printf ("Límite inferior: ");
	scanf ("%lf", &li);
	printf ("Límite superior: ");
	scanf ("%lf", &ls);

	if (li > ls) {  
		double aux = li;   
		li = ls;
		ls = aux;

	}

	double inc = fabs (ls - li);  


	area = integral (pol, dim, li, ls, inc);   
	ultima_area = area - 5 * MAX_ERROR;
	for (inc/=2; fabs (area - ultima_area) > MAX_ERROR; inc/=2) {
		ultima_area = area;
		area = integral (pol, dim, li, ls, inc);
	}

	printf ("\n");
	printf ("El área de:\n");

	printf ("entre [%.2lf, %.2lf] es: \n", li, ls);
	printf ("\nArea = %.3lf\n", area);


	return EXIT_SUCCESS;
}




