#include <stdio.h>
#include <stdlib.h>

#define N 10

int factorial (int n) {
    if ( n == 0 )
        return 1;

    return n * factorial (n-1);
}

double e (unsigned n) { /* Orden de llamadas: e(10)->e(9)->e(8)->e(7)->e(6)->e(5)->e(4)->e(3)->e(2)->e(1)->e(0) */
    if ( n == 0 )       /* Valores de retorn:                                              ... 2.5 <-  2 <-  1  */
        return 1;
    return 1. / factorial (n) + e (n-1);
}

int main (int argc, char *argv[]) {

    printf ("e = %.4lf\n", e (N));

    return EXIT_SUCCESS;
}

