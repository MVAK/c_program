//programmer:Mario Valle Armendariz
//teacher: Txema
//Exercises: make a function to solve e with n numbers

#include <stdio.h>
#include <stdlib.h>

int factorial (int n) {// its a function to make factorial of a number
      if ( n == 0 )
          return 1;
      else return n * factorial (n-1);// make the recursive call factorial to make all times need until n have a value 0.

/* e(0)= 1/0! = 1
e(1)= 1/1! + 1/0! = 2
e(n)=sum(0,n){1/x!}
*/
}
double e (unsigned n) { 
    if ( n == 0 ) 
        return 1;
    return 1. / factorial (n) + e (n-1);//recursive function with factorial 
//with recursive call of e
}
int main (int argc, char *argv[]) {
    int numero;
    printf("hasta que numero quieres hacer el numero e: ");
    scanf("%ui",&numero);
    printf ("e = %.4lf\n", e (numero));

    return EXIT_SUCCESS;
}



 

