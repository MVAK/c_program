#include <stdio.h>
#include <stdlib.h>

const char *command = NULL;

void print_usage () {
    fprintf (stderr, "%s <posible primo>\n", command);
    exit (1);
}

bool divide (           /* Devuelve true si n_divisor o algún número menor que él divide a n_original*/
        int n_original, /* Numero original */
        int n_divisor   /* Número posible divisor */)
{
    if (n_divisor < 2)  /* Condición de salida. El 1 no interesa ahora para ver si un número es primo. */
        return false;

    return  (n_original % n_divisor == 0) || divide (n_original, n_divisor - 1);
    /* Si el divisor divide al original no se hacen más cálculos. De lo contrario hay que llamar a divide
     * para poder saber cuál será el resultado del OR */
}

bool es_primo (int n) {
    return !divide (n, n/2);
}

int main (int argc, char *argv[]) {

    int n;

    command = argv[0];

    if (argc < 2)
        print_usage ();

    n = atoi (argv[1]); /* atoi: ASCII to integer */

    printf ("%i %s es primo.\n",
            n,
            es_primo (n) ? "sí" : "no"
            );

    printf ("\n");

    return EXIT_SUCCESS;
}

