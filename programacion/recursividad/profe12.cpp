#include <stdio.h>
#include <stdlib.h>

#define ERROR 0.0001

bool tienen_el_mismo_signo (double a, double b) { return a * b >= 0; }

double f (double pol[], int grado, double x) {
    double resultado = 0;
    double potencia = 1;

    for (int i = 0; i<=grado; i++, potencia*=x)
        resultado += potencia * pol[i];

    return resultado;
}

void preguntar (double coef[], int grado, double *li, double *ls) {
    double fli = 0, fls = 0;
    do {
        system ("clear");
        system ("toilet -f pagga --gay RAICES");
        printf("\n\n Introduces dos valores de x de distinto signo.\n\n");

        printf ("f(%.2lf) = %.2lf\n", *li, fli);
        printf ("f(%.2lf) = %.2lf\n", *ls, fls);

        printf ("Límite Inferior: ");
        scanf ("%lf", li);
        fli = f(coef, grado, *li);

        printf ("Límite Superior: ");
        scanf ("%lf", ls);
        fls = f(coef, grado, *ls);
    } while (tienen_el_mismo_signo ( fli, fls ) );

    if (li > ls) { /* Si el usuario mete los límites cambiados...  */
        double aux = *li;
        *li = *ls;
        *ls = aux;
    } /* Los intercambiamos */
}

double buscar_cero (double coef[], int grado, double li, double ls) {
    double medio = (ls + li) / 2;

    if (ls - li < ERROR)
       return medio;

    if ( tienen_el_mismo_signo (f(coef, grado, li), f(coef, grado, medio) ) )
        li = medio;
    else
        ls = medio;

    return buscar_cero (coef, grado, li, ls);
}

int main (int argc, char *argv[]) {

    int grado = 2;
    double coef[] = {-48, 8, 1}; /* x² + 8x - 48 : Raíces: -12 y 4 */
    double li, ls;              /* Límite inferior y superior. */

    preguntar (coef, grado, &li, &ls);
    printf (" Hemos encontrado una raíz entre %.2lf y %.2lf está en: %.4lf\n", li, ls,
            buscar_cero (coef, grado, li, ls) );

    return EXIT_SUCCESS;
}

