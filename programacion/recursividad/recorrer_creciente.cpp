#include <stdio.h>
#include <stdlib.h>

void imprime (int n) {

	if (n<0)
		return;

	imprime (n-1);
	printf ("%i ", n);
}

int main (int argc, char *argv[]) {

	int n;
	printf("Cual es el numero mas alto que quieres recorrer descendiente: ");
	scanf("%i",&n);


	imprime (n);

	printf ("\n");

	return EXIT_SUCCESS;
}

