
/*2.1 imprimir cuadrado de * preguntandole el lado al usuario
             2.2 con que caracter quieres que se ponga(preguntar)*/

#include <stdio.h>
#include <stdlib.h>

int altura;
int main(){
	printf("¿cuanto quieres que mida el lado del cuadrado?: ");
	scanf("%i", &altura );
	for (int f = 0 ; f <altura; f++){
		for(int c=0 ; c<altura; c++){
			if (f == 0 || c == 0 || f == altura - 1 || c == altura - 1)
			printf("*");
			else 
				printf(" ");
		}
		printf("\n");
	}



	return EXIT_SUCCESS;
}
