#include "common.h"

#include <math.h>

typedef double var;

/* Funciones auxiliares privadas */

double c (double x) { return x * x; }


/* Funciones escalares */
double deg2rad (double deg) { return deg * M_PI / 180; }
double rad2deg (double rad) { return rad * 180 / M_PI; }

/* Funciones vectoriales */
/*-- cartesianas */
void car2cil (double dst[DIM], double src[DIM]) {
    dst[0] = sqrt (c (src[0]) + c (src[1]) );       /* ρ = √ (x² + y²) */
    dst[1] = atan2 (src[1], src[0]);                /* θ = atan2 ( y/x ) */
    dst[2] = src[2];                                /* z = z */
}

void car2esf (double dst[DIM], double src[DIM]) {
    var ro2 = c (src[0]) + c (src[1]);
    var ro = sqrt (ro2);
    dst[0] = sqrt ( ro2 + c (src[2]) );       /* r = √ (x² + y² + z²) */
    dst[1] = atan2 (src[1], src[0]);          /* θ = atan2 ( y/x ) */
    dst[2] = atan2 ( src[2], ro );            /* φ = atan2 ( z/ρ ) */
                                              /* φ medido desde el plano ecuatorial. */
}


/*-- cilíndricas */
void cil2esf (double dst[DIM], double src[DIM]) {
    dst[0] = sqrt (c (src[0]) + c (src[2]));  /* r = √(ρ² + z²) */
    dst[1] = src[1];                          /* θ = θ */
    dst[2] = atan2 (src[2], src[1]);          /* φ = atan2 ( z/ρ ) */
}

void cil2car (double dst[DIM], double src[DIM]) {
    dst[0] = src[0] * cos (src[1]);           /* x = ρ·cos θ */
    dst[1] = src[0] * sin (src[1]);           /* y = ρ·sin θ */
    dst[2] = src[2];                          /* z = z */
}


/*-- esféricas */
void esf2car (double dst[DIM], double src[DIM]) {
    var ro = src[0] * cos (src[2]);           /* ρ = r·cos φ */
    dst[0] = ro * cos (src[1]);               /* x = ρ·cos θ */
    dst[1] = ro * sin (src[1]);               /* y = ρ·sin θ */
    dst[2] = src[0] * sin (src[2]);           /* z = r·sin φ */
}

void esf2cil (double dst[DIM], double src[DIM]) {
    dst[0] = src[0] * cos (src[2]);           /* ρ = r·cos φ */
    dst[1] = src[1];                          /* θ = θ */
    dst[2] = src[0] * sin (src[2]);           /* z = r·sin φ */
}

