#ifndef __INTERFAZ_H__
#define __INTERFAZ_H__

#include "common.h"
#include "euclides.h"

#ifdef __cplusplus
extern "C" {
#endif
    void title ();
    void print_options (const char * const estadio);
    enum TBase ask_option ();
    void menu (enum TBase *src, enum TBase *dst);
    void ask_vector (enum TBase base, double vector[DIM]);
    void print_result (double src[DIM], double dst[DIM],
            enum TBase srcbas, enum TBase dstbas);
#ifdef __cplusplus
}
#endif

#endif

